﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthBar : MonoBehaviour
{
    public Slider HealthBarSlider;
    public Image healthBarFillImage;

    public void UpdateHealthBarColor()
    {
        healthBarFillImage.color = Color.Lerp(Color.red, Color.green, HealthBarSlider.value);
    }
}
