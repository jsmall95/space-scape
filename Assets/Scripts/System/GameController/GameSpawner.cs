﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

[Serializable]
public class GameSpawnClass
{
    public string className;
    public float spawnChance;
    public GameSpawnObject[] spawnObjects;
}

[Serializable]
public class GameSpawnObject
{
    public string objectName;
    public GameObject spawnObject;
    public float spawnChance;
}

public class GameSpawner : MonoBehaviour
{
    public GameSpawnClass[] spawnClasses;
    public Vector2 spawnValues;

    public int hazardCount;
    public float spawnWait;
    public float startWait;

    public IEnumerator SpawnWaves()
    {
        yield return new WaitForSeconds(startWait);
        while (true)
        {
            for (int i = 0; i < hazardCount; i++)
            {
                GameSpawnClass randomSpawnClass = GetRandomWeightedClass();
                if(randomSpawnClass == null)
                {
                    continue;
                }
                GameSpawnObject randomSpawnObject = GetRandomWeightedObject(randomSpawnClass);
                if(randomSpawnObject == null)
                {
                    continue;
                }

                Vector2 spawnPosition = new Vector2(Random.Range(-spawnValues.x, spawnValues.x), spawnValues.y);
                Quaternion spawnRotation = Quaternion.identity;
                Instantiate(randomSpawnObject.spawnObject, spawnPosition, spawnRotation);
                yield return new WaitForSeconds(spawnWait);
            }
        }
    }

    private GameSpawnClass GetRandomWeightedClass()
    {
        float weightSum = GetClassWeightSum(spawnClasses);

        float randomValue = Random.value;
        float sumValue = 0f;

        for(int i = 0; i < spawnClasses.Length; i++)
        {
            sumValue += spawnClasses[i].spawnChance / weightSum;
            if(sumValue >= randomValue)
            {
                return spawnClasses[i];
            }
        }

        return null;
    }

    private GameSpawnObject GetRandomWeightedObject(GameSpawnClass spawnClass)
    {
        float weightSum = GetObjectWeightSum(spawnClass.spawnObjects);

        float randomValue = Random.value;
        float sumValue = 0f;

        for (int i = 0; i < spawnClass.spawnObjects.Length; i++)
        {
            sumValue += spawnClass.spawnObjects[i].spawnChance / weightSum;
            if (sumValue >= randomValue)
            {
                return spawnClass.spawnObjects[i];
            }
        }

        return null;
    }


    private float GetClassWeightSum(GameSpawnClass[] classes)
    {
        float weightSum = 0f;
        for(int i = 0; i < classes.Length; i++)
        {
            weightSum += classes[i].spawnChance;
        }

        return weightSum;
    }
    private float GetObjectWeightSum(GameSpawnObject[] objects)
    {
        float weightSum = 0f;
        for (int i = 0; i < objects.Length; i++)
        {
            weightSum += objects[i].spawnChance;
        }

        return weightSum;
    }
}
