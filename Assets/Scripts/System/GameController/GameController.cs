﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

[RequireComponent(typeof(GameSpawner))]
public class GameController : MonoBehaviour
{
    public static GameController instance = null;

    public TextMeshProUGUI scoreText;
    public GameObject GameOverScreen;
    public AudioClip GameOverMusic;

    private AudioSource _audioSource;
    private int score;
    private GameSpawner _gameSpawner;

    private bool gameOver = false;

    private void Awake()
    {
        if(instance == null)
        {
            instance = this;
        }
        else if(instance != this)
        {
            Destroy(gameObject);
        }
        _audioSource = GetComponent<AudioSource>();
        _gameSpawner = GetComponent<GameSpawner>();
    }

    // Start is called before the first frame update
    void Start()
    {
        score = 0;
        UpdateScore();
        StartCoroutine(_gameSpawner.SpawnWaves());
    }

    public void AddScore(int scoreValue)
    {
        score += scoreValue;
        UpdateScore();
    }

    public void GameOver()
    {
        GameOverScreen.SetActive(true);
        _audioSource.clip = GameOverMusic;
        _audioSource.loop = false;
        _audioSource.Play();
        gameOver = true;
        StopCoroutine(_gameSpawner.SpawnWaves());
    }

    public void ReloadLevel()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    private void UpdateScore()
    {
        scoreText.text = "Score: " + score;
    }
}
