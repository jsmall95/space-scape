﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Rigidbody2D))]
public class EnemyBase : MonoBehaviour, IDestroyedByBoundary, IDamagedByProjectile, IGivePoints
{
    public int maxHealth = 1;
    protected int currentHealth;

    protected Rigidbody2D _rb;
    [SerializeField]
    private GameObject destroyedEffect;
    public GameObject DestroyedEffect
    {
        get { return destroyedEffect; }
    }

    [SerializeField]
    private int pointValue;
    public int PointValue
    {
        get { return pointValue; }
    }

    [SerializeField]
    private HealthBar healthBar;
    public HealthBar HealthBar { get { return healthBar; } }

    protected void Awake()
    {
        _rb = GetComponent<Rigidbody2D>();
        currentHealth = maxHealth;
    }

    public void DestroyByBoundary()
    {
        Destroy(gameObject);
    }

    public virtual void DamageObject()
    {
        currentHealth--;
        if (currentHealth <= 0)
        {
            DestroyObject();
            return;
        }
        HealthBar.HealthBarSlider.value = Mathf.Clamp01((float)currentHealth / maxHealth);
    }

    public virtual void DestroyObject()
    {
        if (DestroyedEffect != null)
        {
            Instantiate(DestroyedEffect, transform.position, transform.rotation);
        }
        Destroy(gameObject);
    }
}
