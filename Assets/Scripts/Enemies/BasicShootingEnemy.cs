﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(WeaponController))]
public class BasicShootingEnemy : EnemyBase
{
    public float speed;
    protected void Awake()
    {
        base.Awake();
        _rb.velocity = transform.up * speed;
    }
}
