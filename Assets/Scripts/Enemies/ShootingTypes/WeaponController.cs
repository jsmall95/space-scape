﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponController : MonoBehaviour
{
    public GameObject shot;
    public ShotSpawn[] shotSpawn;
    public ShotSpawn ShotSpawn
    {
        get
        {
            return shotSpawn[0];
        }
    }
    public float fireRate;
    public float delay;

    protected AudioSource _audio;
    // Start is called before the first frame update
    protected virtual void Start()
    {
        _audio = GetComponent<AudioSource>();
        InvokeRepeating("Fire", delay, fireRate);
    }

    protected virtual void Fire()
    {
        Vector2 shotPosition = ShotSpawn.transform.position;
        if (ShotSpawn.dualCannons)
        {
            shotPosition.x = ShotSpawn.IsLeft ? (shotPosition.x - ShotSpawn.symmetricSpacing) : (shotPosition.x + ShotSpawn.symmetricSpacing);
        }
        Instantiate(shot, shotPosition, ShotSpawn.transform.rotation);
        _audio.Play();
    }
}