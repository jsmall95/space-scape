﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SprayWeaponController : WeaponController
{
    // Start is called before the first frame update
    protected override void Start()
    {
        base.Start();   
    }

    protected override void Fire()
    {
        foreach(var spawn in shotSpawn)
        {
            Vector2 shotPosition = spawn.transform.position;
            Instantiate(shot, shotPosition, spawn.transform.rotation);
        }
        _audio.Play();
    }
}
