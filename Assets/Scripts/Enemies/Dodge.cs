﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public struct MinMaxTime
{
    public float min;
    public float max;
}

[RequireComponent(typeof(Rigidbody2D))]
public class Dodge : MonoBehaviour
{
    public MinMaxTime startWait;
    public MinMaxTime dodgeTime;
    public MinMaxTime dodgeWait;
    public PlayerBoundary boundary = new PlayerBoundary()
    {
        xMin = -4.5f,
        xMax = 4.5f,
        yMin = -11f,
        yMax = 11f
    };

    public float dodge;
    public float smoothing;

    private float targetDodge;

    private Rigidbody2D _rb;
    
    private void Start()
    {
        _rb = GetComponent<Rigidbody2D>();
        StartCoroutine(DodgeRoutine());
    }

    private void FixedUpdate()
    {
        float newDodge = Mathf.MoveTowards(_rb.velocity.x, targetDodge, Time.deltaTime * smoothing);
        _rb.velocity = new Vector2(newDodge, _rb.velocity.y);
        HandleShipPosition();
    }

    IEnumerator DodgeRoutine()
    {
        yield return new WaitForSeconds(Random.Range(startWait.min, startWait.max));

        while (true)
        {
            targetDodge = Random.Range(1, dodge * -Mathf.Sign(transform.position.x));
            yield return new WaitForSeconds(Random.Range(dodgeTime.min, dodgeTime.max));
            targetDodge = 0;
            yield return new WaitForSeconds(Random.Range(dodgeWait.min, dodgeWait.max));
        }
    }

    private void HandleShipPosition()
    {
        _rb.position = new Vector2(
            Mathf.Clamp(_rb.position.x, boundary.xMin, boundary.xMax),
            Mathf.Clamp(_rb.position.y, boundary.yMin, boundary.yMax));

    }
}
