﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthManager : MonoBehaviour, ITakeDamage
{
    public int MaxHealth;
    public float invulnerabilityTime = 1f;
    public GameObject DestroyedEffect;
    public Image healthBar;

    private float currentInvulnerabilityTime = 0;
    private int currentHealth;
    private bool IsDead = false;

    private Animator anim;
    private Collider2D col;
    private PlayerController playerController;

    private bool hitsEnabled = true;

    // Start is called before the first frame update
    void Start()
    {
        currentHealth = MaxHealth;
        anim = GetComponent<Animator>();
        col = GetComponent<Collider2D>();
        playerController = GetComponent<PlayerController>();
        healthBar.fillAmount = Mathf.Clamp01((float)currentHealth / MaxHealth);
    }

    // Update is called once per frame
    void Update()
    {
        if(currentHealth <= 0)
        {
            IsDead = true;
        }

        if(currentInvulnerabilityTime > 0)
        {
            currentInvulnerabilityTime -= Time.deltaTime;
            if (!anim.GetCurrentAnimatorStateInfo(0).IsName("PlayerInvunerable"))
            {
                anim.Play("PlayerInvunerable");
            }
        }
        else if(hitsEnabled)
        {
            col.enabled = true;
        }
    }

    public void TakeDamage(int dmg)
    {
        if (!IsDead && currentHealth > 0 && currentInvulnerabilityTime <= 0)
        {
            currentHealth -= dmg;
            currentInvulnerabilityTime = invulnerabilityTime;
            col.enabled = false;
            healthBar.fillAmount = Mathf.Clamp01((float)currentHealth / MaxHealth);
            if (currentHealth <= 0)
            {
                KillPlayer();
            }
        }
    }

    public void KillPlayer()
    {
        col.enabled = false;
        playerController.DisablePlayer();
        if(DestroyedEffect != null)
        {
            Instantiate(DestroyedEffect, transform.position, transform.rotation);
        }
        GameController.instance.GameOver();
    }

    public void DisableHits()
    {
        hitsEnabled = false;
    }

    public void EnableHits()
    {
        hitsEnabled = true;
    }
}
