﻿using System;
using UnityEngine;

[Serializable]
public class PlayerBoundary
{
    public float xMin, xMax, yMin, yMax;
}

public class PlayerController : MonoBehaviour
{
    //Ship Stats
    public float speed = 1;
    public float fireRate = 0.5f;
    private float nextFire = 0.0f;

    private bool playerEnabled = true;

    public Projectile basicShotProjectile;
    public ShotSpawn basicShotSpawn;
    public PlayerBoundary boundary;

    private Vector2 currentDirection;

    private Quaternion calibrationQuaternion;
    private AudioSource _audio;

    private float movementOffsetX, movementOffsetY;


    private void Awake()
    {
        _audio = GetComponent<AudioSource>();
        Input.multiTouchEnabled = false;
        currentDirection = Vector2.zero;
    }

    private void Update()
    {
        if (playerEnabled)
        {
            HandleTouch();
            Move();
            Shoot();
        }
        HandleShipPosition();
    }

    private void HandleTouch()
    {
        if(Input.touchCount > 0)
        {
            Touch touch = Input.GetTouch(0);
            Vector2 touchPos = Camera.main.ScreenToWorldPoint(touch.position);

            switch (touch.phase)
            {
                case TouchPhase.Began:
                    movementOffsetX = touchPos.x - transform.position.x;
                    movementOffsetY = touchPos.y - transform.position.y;
                    break;
                case TouchPhase.Stationary:
                    currentDirection = Vector2.zero;
                    break;
                case TouchPhase.Moved:
                    var moveLocation = new Vector2(touchPos.x - movementOffsetX, touchPos.y - movementOffsetY);
                    currentDirection = moveLocation - (Vector2)transform.position;
                    break;
                case TouchPhase.Ended:
                    currentDirection = Vector2.zero;
                    break;

            }
        }
    }

    public void DisablePlayer()
    {
        playerEnabled = false;
        currentDirection = Vector2.zero;
    }

    public void EnablePlayer()
    {
        playerEnabled = true;
        currentDirection = Vector2.zero;
    }

    private void Move()
    {
        gameObject.transform.position += (Vector3)currentDirection * speed * Time.fixedDeltaTime;
    }

    private void Shoot()
    {
        if(Time.time > nextFire)
        {
            nextFire = Time.time + fireRate;
            Vector2 shotPosition = basicShotSpawn.transform.position;
            if (basicShotSpawn.dualCannons)
            {
                shotPosition.x = basicShotSpawn.IsLeft ? (shotPosition.x - basicShotSpawn.symmetricSpacing) : (shotPosition.x + basicShotSpawn.symmetricSpacing);
            }

            Projectile projectileClone = Instantiate(basicShotProjectile, shotPosition, basicShotSpawn.transform.rotation);
            _audio.Play();
        }
    }
    
    private void HandleShipPosition()
    {
        gameObject.transform.position = new Vector2(
            Mathf.Clamp(gameObject.transform.position.x, boundary.xMin, boundary.xMax),
            Mathf.Clamp(gameObject.transform.position.y, boundary.yMin, boundary.yMax));

    }
}
