﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(CircleCollider2D))]
[RequireComponent(typeof(Rigidbody2D))]
public class PowerUpBase : MonoBehaviour, IDestroyedByBoundary
{
    public GameObject PowerUpEffect;
    public float speed;

    protected Rigidbody2D _rb;

    protected void Awake()
    {
        _rb = GetComponent<Rigidbody2D>();
        _rb.velocity = transform.up * speed;
    }

    protected virtual void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.CompareTag("Player")){
            Debug.Log("You should add a real power up");
        }
    }

    public virtual void DestroyByBoundary()
    {
        Destroy(gameObject);
    }
}
