﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShieldPowerUp : PowerUpBase
{
    public float shieldTime;

    private bool used = false;
    private GameObject player;
    protected override void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Player") && !used)
        {
            used = true;
            player = collision.gameObject;
            GetComponentInChildren<SpriteRenderer>().enabled = false;
            GetComponent<CircleCollider2D>().enabled = false;
            StartCoroutine(ShieldPlayer());
        }
    }

    IEnumerator ShieldPlayer()
    {
        if(player != null)
        {
            var playerHealthManager = player.GetComponent<HealthManager>();
            if(playerHealthManager != null)
            {
                playerHealthManager.DisableHits();
                var powerUp = Instantiate(PowerUpEffect, player.transform.position, player.transform.rotation);
                powerUp.transform.parent = player.transform;
                yield return new WaitForSeconds(shieldTime);
                Destroy(powerUp);
                playerHealthManager.EnableHits();
            }
        }
        Destroy(gameObject);
    }

    public override void DestroyByBoundary()
    {
        if (!used)
        {
            Destroy(gameObject);
        }
    }
}
