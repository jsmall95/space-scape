﻿using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine;

public class SimpleTouchPad : MonoBehaviour, IPointerDownHandler, IDragHandler, IPointerUpHandler
{
    public float smoothing;

    private Vector2 origin;
    private Vector2 direction;
    private Vector2 smoothedDirection;

    private bool touched;
    private int pointerID;

    private void Awake()
    {
        direction = Vector2.zero;
        touched = false;
    }

    public void OnDrag(PointerEventData eventData)
    {
        if(eventData.pointerId == pointerID)
        {
            Vector2 currentPosition = eventData.position;
            Vector2 directionRaw = currentPosition - origin;
            direction = directionRaw.normalized;
        }
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        if (!touched)
        {
            touched = true;
            pointerID = eventData.pointerId;
            origin = eventData.position;
        }
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        if(eventData.pointerId == pointerID)
        {
            touched = false;
            direction = Vector2.zero;
        }
    }

    public Vector2 GetDirection()
    {
        return direction;
    }

    public Vector2 GetSmoothedDirection()
    {
        smoothedDirection = Vector2.MoveTowards(smoothedDirection, direction, smoothing);
        return smoothedDirection;
    }
}
