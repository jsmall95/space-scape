﻿using UnityEngine;

public class CircleExpandProjectile : MonoBehaviour
{
    public Projectile projectile;
    public int maxProjectiles;
    public float startRadius = 0.05f;

    public void SpawnCircle(float radius, int numProjectiles)
    {
        startRadius = radius;
        maxProjectiles = numProjectiles;
        SpawnCircleOfProjectiles();
        Destroy(gameObject);
    }

    private void SpawnCircleOfProjectiles()
    {
        for(int i = 0; i < maxProjectiles; i++)
        {
            Vector2 pos = GetSpawnLocation(i);
            Vector2 difference = pos - (Vector2)transform.position;
            float rotationZ = Mathf.Atan2(difference.y, difference.x) * Mathf.Rad2Deg;
            Instantiate(projectile, pos, Quaternion.Euler(0f, 0f, rotationZ));
        }
    }

    private Vector2 GetSpawnLocation(int index)
    {
        float ang = (int)(360 / maxProjectiles) * index;
        Vector2 pos;
        pos.x = transform.position.x + startRadius * Mathf.Sin(ang * Mathf.Deg2Rad);
        pos.y = transform.position.y + startRadius * Mathf.Cos(ang * Mathf.Deg2Rad);
        return pos;
    }
}
