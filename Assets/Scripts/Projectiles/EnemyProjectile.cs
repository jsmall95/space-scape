﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyProjectile : Projectile
{
    public int damage;
    public string targetTag;
    protected override void OnTriggerEnter2D(Collider2D collision)
    {
        var takeDamage = collision.GetComponent<ITakeDamage>();
        if (takeDamage != null && collision.CompareTag(targetTag))
        {
            takeDamage.TakeDamage(damage);
            Destroy(gameObject);
        }
    }
}
