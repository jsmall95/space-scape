﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShotSpawn : MonoBehaviour
{
    public bool dualCannons;
    public float symmetricSpacing;
    private bool isLeft = true;

    public bool IsLeft
    {
        get {
            isLeft = !isLeft;
            return isLeft;
        }
    } 
}
