﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
public class Projectile : MonoBehaviour, IDestroyedByBoundary
{
    public float speed = 1f;

    private Rigidbody2D _rb;

    // Start is called before the first frame update
    void Start()
    {
        _rb = GetComponent<Rigidbody2D>();
        _rb.velocity = transform.up * speed;
    }

    public void DestroyByBoundary()
    {
        Destroy(gameObject);
    }

    protected virtual void OnTriggerEnter2D(Collider2D collision)
    {
        var destroyable = collision.GetComponent<IDamagedByProjectile>();
        if(destroyable != null)
        {
            var givesPoints = collision.GetComponent<IGivePoints>();
            if (givesPoints != null)
            {
                GameController.instance.AddScore(givesPoints.PointValue);
            }
            destroyable.DamageObject();
            Destroy(gameObject);
        }
    }
}
