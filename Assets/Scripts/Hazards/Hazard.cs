﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Rigidbody2D))]
public class Hazard : MonoBehaviour, IDestroyedByBoundary, IDamagedByProjectile, IGivePoints
{
    public float speed = 1f;

    [SerializeField]
    private GameObject destroyedEffect;
    public GameObject DestroyedEffect
    {
        get { return destroyedEffect; }
    }

    [SerializeField]
    private int pointValue;
    public int PointValue
    {
        get { return pointValue; }
    }

    [SerializeField]
    protected HealthBar healthBar;
    public HealthBar HealthBar { get { return healthBar; } }

    private Rigidbody2D _rb;

    protected void Start()
    {
        _rb = GetComponent<Rigidbody2D>();
        _rb.velocity = transform.up * speed;
    }

    public void DestroyByBoundary()
    {
        Destroy(gameObject);
    }

    public virtual void DamageObject()
    {
        DestroyObject();
    }

    public virtual void DestroyObject()
    {
        if (DestroyedEffect != null)
        {
            Instantiate(DestroyedEffect, transform.position, transform.rotation);
        }
        Destroy(gameObject);
    }
}
