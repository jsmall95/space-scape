﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DamageOnHit : MonoBehaviour
{
    public int Damage;
    public string TargetTag;
    private IDamagedOnHit destroyOnHit;

    private void Start()
    {
        destroyOnHit = GetComponent<IDamagedOnHit>();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        var takeDamage = collision.GetComponent<ITakeDamage>();
        if(takeDamage != null && collision.CompareTag(TargetTag))
        {
            takeDamage.TakeDamage(Damage);
            if (destroyOnHit != null)
            {
                destroyOnHit.DamageObject();
            }
        }
    }
}
