﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
public class RandomRotator : MonoBehaviour
{
    public float tumble;

    private Rigidbody2D _rb;

    // Start is called before the first frame update
    void Start()
    {
        _rb = GetComponent<Rigidbody2D>();
        _rb.angularVelocity = Random.Range(-20, 20) * tumble;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
