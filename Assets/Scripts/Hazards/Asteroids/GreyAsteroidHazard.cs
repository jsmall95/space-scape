﻿using UnityEngine;

public class GreyAsteroidHazard : AsteroidHazard
{
    public AsteroidHazard spawnedAsteroid;
    public float radius = 0.05f;
    public int numberOfAsteroids = 8;
    public override void DamageObject()
    {
        base.DamageObject();
    }

    public override void DestroyObject()
    {
        SpawnCircleOfAsteroids();
        base.DestroyObject();
    }

    private void SpawnCircleOfAsteroids()
    {
        for (int i = 0; i < numberOfAsteroids; i++)
        {
            Vector2 pos = GetSpawnLocation(i);
            Vector2 difference = pos - (Vector2)transform.position;
            float rotationZ = Mathf.Atan2(difference.y, difference.x) * Mathf.Rad2Deg;
            Instantiate(spawnedAsteroid, pos, Quaternion.Euler(0f, 0f, rotationZ));
        }
    }

    private Vector2 GetSpawnLocation(int index)
    {
        float ang = (int)(360 / numberOfAsteroids) * index;
        Vector2 pos;
        pos.x = transform.position.x + radius * Mathf.Sin(ang * Mathf.Deg2Rad);
        pos.y = transform.position.y + radius * Mathf.Cos(ang * Mathf.Deg2Rad);
        return pos;
    }
}
