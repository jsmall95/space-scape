﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Asteroid Type", menuName = "Hazards")]
public class AsteroidHazardTypes : ScriptableObject
{
    public string AsteroidType;
    public float SpawnChance = 1f;
}
