﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AsteroidHazard : Hazard
{
    public int maxHits = 1;

    private int currentHitsLeft;

    private new void Start()
    {
        base.Start();
        currentHitsLeft = maxHits;
    }

    public override void DamageObject()
    {
        currentHitsLeft--;
        if(currentHitsLeft <= 0)
        {
            DestroyObject();
            return;
        }
        else
        {
            HealthBar.HealthBarSlider.value = Mathf.Clamp01((float)currentHitsLeft / maxHits);
        }
    }
}
