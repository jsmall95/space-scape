﻿public class RedAsteroidHazard : AsteroidHazard
{
    public CircleExpandProjectile circleExpandProjectile;
    public float radius = 0.05f;
    public int numberOfProjectiles = 8;
    public override void DamageObject()
    {
        base.DamageObject();
    }

    public override void DestroyObject()
    {
        CircleExpandProjectile explosion = Instantiate(circleExpandProjectile, transform.position, transform.rotation);
        explosion.SpawnCircle(radius, numberOfProjectiles);
        base.DestroyObject();
    }
}
