﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomAsteroidHazard : AsteroidHazard
{
    public AsteroidHazardTypes[] asteroidTypes;

    public override void DamageObject()
    {
        base.DamageObject();
    }

    public override void DestroyObject()
    {
        var randomValue = Random.Range(0, asteroidTypes.Length);
        var asteroidType = asteroidTypes[randomValue];

        AsteroidHazard randomHazard = GetComponent(asteroidType.AsteroidType) as AsteroidHazard;

        if(randomHazard != null)
        {
            randomHazard.DestroyObject();
        }
        else
        {
            base.DestroyObject();
        }
    }
}
