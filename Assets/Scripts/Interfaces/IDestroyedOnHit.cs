﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public interface IDamagedOnHit
{
    HealthBar HealthBar { get; }
    GameObject DestroyedEffect { get; }
    void DamageObject();
}
