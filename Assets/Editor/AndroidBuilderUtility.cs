﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public static class AndroidBuilderUtility
{
    public static void UpdateBuildVersion()
    {
        PlayerSettings.Android.bundleVersionCode++;
    }

    public static string GetFullVersion()
    {
        return PlayerSettings.bundleVersion + "." + PlayerSettings.Android.bundleVersionCode;
    }
}
