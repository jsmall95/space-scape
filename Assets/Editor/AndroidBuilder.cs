﻿using System;
using UnityEditor;
using UnityEditor.Build.Reporting;

public class AndroidBuilder
{
    [MenuItem("MyTools/Android Build Dev")]
    static void buildDev()
    {
        string[] scenes = {
            "Assets/Scenes/MainMenu.unity",
            "Assets/Scenes/Level1.unity"
        };

        string pathToDeploy = "D:\\Jesse\\D stuff\\My Documents\\Google Drive\\Space Scape Builds\\dev\\" + AndroidBuilderUtility.GetFullVersion() + "\\com.JSmallz.ScapeSpace.apk";

        BuildPipeline.BuildPlayer(scenes, pathToDeploy, BuildTarget.Android, BuildOptions.None);
    }

    [MenuItem("MyTools/Android Build")]
    static void build()
    {
        string[] scenes = {
            "Assets/Scenes/MainMenu.unity",
            "Assets/Scenes/Level1.unity"
        };

        string pathToDeploy = "D:\\Jesse\\D stuff\\My Documents\\Google Drive\\Space Scape Builds\\master\\" + AndroidBuilderUtility.GetFullVersion() + "\\com.JSmallz.ScapeSpace.apk";

        BuildPipeline.BuildPlayer(scenes, pathToDeploy, BuildTarget.Android, BuildOptions.None);
    }
}
