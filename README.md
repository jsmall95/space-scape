**Space Scape**

![Space Scape Image](https://imgur.com/7EnT85l.jpg)

**Release Schedule**
I plan on creating a new downloadable release for this game every 2 weeks (about the 15th and 30th of every month).

**Why did you start this project?**
I wanted to create a game that was simple, fun and easy to produce.
I always try to create games with such large scopes that it becomes really hard to finish them while maintaining a career and personal relationships.
This project was started with a narrow scope in mind and is a fun way to waste some time on weekends.

**How big is the scope of this project?**
I want to create something that is simple and fun to play.
I have ideas of how to expand the scope of this project such as ship upgrades, but I am not going to pursue this until the game is in a finished state first.

**What are the main goals/features you want to hit with this game?**
* Basic Enemies
    *  Suiciders
    *  Shooters
    *  Smart Shooters
* Bosses
    * 1 Unique Boss Fight
    * 
* Main Menu
    * Start New Game
    * Load Existing Game
* Player
    * Basic weapon shot
    * Advanced weapon shot
    * Upgrade powers
        * Nuke
        * Shield
        * Increased Speed
